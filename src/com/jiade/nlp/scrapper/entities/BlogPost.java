package com.jiade.nlp.scrapper.entities;

import java.util.ArrayList;
import java.util.Date;

public class BlogPost {
	private String source;
	private Date date;
	private String title;
	private String text;
	private String details;
	private ArrayList<String> tags;
	/**
	 * @param source
	 * @param date
	 * @param title
	 * @param text
	 * @param details
	 * @param tags
	 */
	public BlogPost(String source, Date date, String title, String text,
			String details, ArrayList<String> tags) {
		super();
		this.source = source;
		this.date = date;
		this.title = title;
		this.text = text;
		this.details = details;
		this.tags = tags;
	}
	public BlogPost() {
		tags = new ArrayList<String>();
	}
	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return the tags
	 */
	public ArrayList<String> getTags() {
		return tags;
	}
	/**
	 * @param tags the tags to set
	 */
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	
}
