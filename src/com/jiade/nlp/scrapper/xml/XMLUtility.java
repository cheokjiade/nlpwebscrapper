package com.jiade.nlp.scrapper.xml;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jiade.nlp.scrapper.entities.BlogPost;

public class XMLUtility {
	private static int count=1, totalWords=0;
	public static void createXML(BlogPost bp){
		try {
			 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("doc");
			//rootElement.setAttribute("id", Integer.toString(count));
			Attr attr = doc.createAttribute("id");
			attr.setValue(Integer.toString(count));
			rootElement.setAttributeNode(attr);
			doc.appendChild(rootElement);
	 
			// staff elements
			Element eSource = doc.createElement("source");
			eSource.appendChild(doc.createTextNode(bp.getSource()));
			rootElement.appendChild(eSource);
			
			Element eDate = doc.createElement("date");
			eDate.appendChild(doc.createTextNode(bp.getDate().toString()));
			rootElement.appendChild(eDate);
			
			Element eTitle = doc.createElement("title");
			eTitle.appendChild(doc.createTextNode(bp.getTitle()));
			rootElement.appendChild(eTitle);
			
			Element eText = doc.createElement("text");
			eText.appendChild(doc.createTextNode(bp.getText()));
			rootElement.appendChild(eText);
			
			Element eTags = doc.createElement("tags");
			for (String tag:bp.getTags()){
				Element tmp = doc.createElement("tag");
				tmp.setTextContent(tag);
				eTags.appendChild(tmp);
			}
			rootElement.appendChild(eTags);
			
			String[] tokenArray = bp.getText().split(" ");
			
			Element eStatistics = doc.createElement("statistics");
			Element tokenCount = doc.createElement("tokencount");
			tokenCount.setTextContent(Integer.toString(tokenArray.length));
			eStatistics.appendChild(tokenCount);
			Element uniqueTokens = doc.createElement("uniquetokens");
			//String[] unique = (String[]) new HashSet<String>(Arrays.asList(tokenArray)).toArray();
			uniqueTokens.setTextContent(Integer.toString(new HashSet<String>(Arrays.asList(tokenArray)).toArray().length));
			eStatistics.appendChild(uniqueTokens);
			rootElement.appendChild(eStatistics);
			
			
	 
			// set attribute to staff element
//			Attr attr = doc.createAttribute("id");
//			attr.setValue("1");
//			staff.setAttributeNode(attr);
	 
			// shorten way
			// staff.setAttribute("id", "1");
	 
			// firstname elements
//			Element firstname = doc.createElement("firstname");
//			firstname.appendChild(doc.createTextNode("yong"));
//			eSource.appendChild(firstname);
//	 
//			// lastname elements
//			Element lastname = doc.createElement("lastname");
//			lastname.appendChild(doc.createTextNode("mook kim"));
//			eSource.appendChild(lastname);
//	 
//			// nickname elements
//			Element nickname = doc.createElement("nickname");
//			nickname.appendChild(doc.createTextNode("mkyong"));
//			eSource.appendChild(nickname);
//	 
//			// salary elements
//			Element salary = doc.createElement("salary");
//			salary.appendChild(doc.createTextNode("100000"));
//			eSource.appendChild(salary);
	 
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			
			Transformer transformer = transformerFactory.newTransformer();
			//for proper indentation
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			
			File f = new File("xmldocs/" + bp.getTitle().replaceAll("[^a-zA-Z0-9.-]", "_") + ".xml");
			if(f.exists()){
		          System.out.println("File exists");
		      }else{
		    	  StreamResult result = new StreamResult(f);
		    		 
					// Output to console for testing
					// StreamResult result = new StreamResult(System.out);
			 
					transformer.transform(source, result);
			 
					System.out.println("File saved!");
					count++;
					totalWords+=tokenArray.length;
					System.out.println("Total Words " + Integer.toString(totalWords));
		      }
			
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
	}
}
