package com.jiade.nlp.scrapper.db;

import java.io.IOException;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.jiade.nlp.scrapper.entities.BlogPost;


public class Db4oHelper {
	private static Db4oHelper singleton = null;
	private static ObjectContainer db = null;

	protected Db4oHelper(){
	}

	public static Db4oHelper getInstance(){
		if (singleton==null){
			singleton = new Db4oHelper();
		}
		return singleton;
	}

	public static ObjectContainer db() {

		try {
			if (db == null || db.ext().isClosed()) {
				db = Db4oEmbedded.openFile(dbConfig(), "BlogPosts.db");
				//We first load the initial data from the database
				//ExercisesLoader.load(context, db);                                         
			}

			return db;

		} catch (Exception ie) {
			ie.printStackTrace();
			return null;
		}
	}
	
	/**
	    * Configure the behavior of the database
	    */

	    private static EmbeddedConfiguration dbConfig() throws IOException {
	           EmbeddedConfiguration configuration = Db4oEmbedded.newConfiguration();
	           configuration.common().objectClass(BlogPost.class).cascadeOnUpdate(true);
	           configuration.common().objectClass(BlogPost.class).cascadeOnDelete(true);
	           return configuration;
	    }
}
