package com.jiade.nlp.scrapper;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.jiade.nlp.scrapper.db.Db4oHelper;
import com.jiade.nlp.scrapper.entities.BlogPost;
import com.jiade.nlp.scrapper.xml.XMLUtility;

/**
 * 
 */

/**
 * @author Me
 *
 */
public class MainScrapper {

	//private static DateFormat df;
	private static DateTimeFormatter formatter;
	//static int corpus = 0;
	static ArrayList<String> errorLinks;
	static ArrayList<String> crawledLinks;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//2013-09-12T10:28:00+08:00
		//df = new SimpleDateFormat("yyyy-MM-dd'T'k:m:sZZZ");
		formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'H:m:sZ");
		errorLinks = new ArrayList<String>();
		crawledLinks = new ArrayList<String>();
		//System.out.println(df.format(new Date()));
		try {
			Document doc = Jsoup.connect("http://ieatishootipost.sg").get();
			Element blogPostsLinksContainer = doc.getElementById("Label3");
			//get list items. each li contains 1 link
			Elements blogPostsLinks = blogPostsLinksContainer.getElementsByTag("li");
			//Get all categories links
			for(Element linkLI : blogPostsLinks){
				Element linkElement = linkLI.getElementsByTag("a").get(0);
				System.out.println("Getting " + linkElement.text() + "\n\n");
				Document blogPostDoc =null;
				try{
					blogPostDoc = Jsoup.connect(linkElement.attr("href")).get();
				}catch (IOException io){
					//blogPostDoc = Jsoup.connect(linkElement.attr("href")).get();
				}finally{
					if(blogPostDoc!=null){
						try{
						handleCategory(blogPostDoc);
						}catch(Exception e){
							e.printStackTrace();
							errorLinks.add(linkElement.attr("href"));
						}
					}
				}
				Db4oHelper.getInstance().db().close();
				//handleCategory(blogPostDoc);
				// remove return for actual purposes
				//return;
				
			}
			
			//System.out.println(doc.toString());
			
			Elements posts = doc.getElementsByClass("hentry");
			for(Element post : posts){
				Elements postTitle = post.getElementsByClass("post-title");
				System.out.println(postTitle.get(0).getElementsByTag("a").get(0).text());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(String errorLink:errorLinks)
			System.out.println(errorLink);
		
	}
	
	public static void handleCategory(Document blogPostDoc){
		Elements posts = blogPostDoc.getElementsByClass("hentry");
		//no posts so return to scrap another category
		if(posts.size()==0) return;
		for(Element post : posts){
			Elements postTitle = post.getElementsByClass("post-title");
			String postURL = postTitle.get(0).getElementsByTag("a").get(0).attr("href");
			if(crawledLinks.contains(postURL)){
				break;
			}
			
			handlePost(postURL);
			
		}
		
		//go to the older page
		Element olderDoc = blogPostDoc.getElementById("blog-pager-older-link");
		Element olderDoclinkElement = olderDoc.getElementsByTag("a").get(0);
		try {
			Document olderBlogPostDoc = Jsoup.connect(olderDoclinkElement.attr("href")).get();
			//recursively handle older pages until all posts from category are read
			handleCategory(olderBlogPostDoc);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void handlePost(String pageURL){
		Document blogPostDoc =null;
		try{
			blogPostDoc = Jsoup.connect(pageURL).get();
		}catch (IOException io){
			//blogPostDoc = Jsoup.connect(linkElement.attr("href")).get();
			return;
		}
		Element post = blogPostDoc.getElementsByClass("hentry").get(0);
		//create new blogpost object for storage.
		BlogPost bp = new BlogPost();
		//each post title is contained in a post-title class div
		Elements postTitle = post.getElementsByClass("post-title");
		//System.out.println(postTitle.get(0).getElementsByTag("a").get(0).text());
		//the div contains 1 link (a) tag, the text is the title and href is the source
		bp.setTitle(postTitle.get(0).text());
		bp.setSource(pageURL);
		Element body = post.getElementsByClass("post-body").get(0);
		//remove span tags as they make the post irrelevent
		body.getElementsByTag("span").remove();
		//body.removeClass(className);
		bp.setText(body.text());
		Element postTimeClass = post.getElementsByClass("timestamp-link").get(0);
		String timeText = postTimeClass.getElementsByTag("abbr").get(0).attr("title");
		//Date d = df.parse(timeText);
		Date d = formatter.parseDateTime(timeText).toDate();
		//System.out.println(d.toString());
		bp.setDate(d);
		
		Element tagDiv = post.getElementsByClass("post-labels").get(0);
		//Elements tagLinks = tagDiv.getElementsByTag("a");
		for(Element tagLink :  tagDiv.getElementsByTag("a")){
			bp.getTags().add(tagLink.text());
		}
		System.out.println(bp.getTitle());
		System.out.println(bp.getSource()!=null?bp.getSource():"");
		System.out.println(bp.getDate()!=null?bp.getDate().toString():"");
		for(String tag : bp.getTags()){
			System.out.println(tag);
		}
		System.out.println();
		System.out.println(bp.getText());
		crawledLinks.add(pageURL);
		XMLUtility.createXML(bp);
		Db4oHelper.getInstance().db().store(bp);
		//corpus += bp.getText().split(" ").length;
		//System.out.println("Total words : " + Integer.toString(corpus));
		System.out.println();
		System.out.println();
	}
	
	

}
